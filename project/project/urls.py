from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
	url(r'^$', tracker_app.views.home, name='home'),
    url(r'^edit_task/$', tracker_app.views.edit_task, name='add_task'),
    url(r'^edit_task/(?P<task_id>[0-9]+)$', tracker_app.views.edit_task, name='edit_task'),
    url(r'^detail/(?P<task_id>[0-9]+)$', tracker_app.views.detail, name='detail'),
    url(r'^subtask/(?P<stask_id>[0-9]+)$', tracker_app.views.edit_task, name='subtask'),
    url(r'^in_tlist/(?P<tlist_id>[0-9]+)$', tracker_app.views.edit_task, name='in_tlist'),
    url(r'^(?P<task_id>[0-9]+)/task_action/', tracker_app.views.task_action, name='task_action'),
    url(r'^(?P<tlist_id>[0-9]+)/tlist_action/', tracker_app.views.tlist_action, name='tlist_action'),
    url(r'^edit_tlist/$', tracker_app.views.edit_tlist),
    url(r'^edit_tlist/(?P<tlist_id>[0-9]+)$', tracker_app.views.edit_tlist, name='edit_tlist'),
    url(r'^registration/', tracker_app.views.registration, name='registration'),
    url(r'^login/', tracker_app.views.login, name='login'),
    url(r'^logout/', tracker_app.views.logout, name='logout'),
    url(r'^share_tlist/(?P<tlist_id>[0-9]+)$', tracker_app.views.share_tlist, name='share_tlist'),
    url(r'^share_task/(?P<task_id>[0-9]+)$', tracker_app.views.share_task, name='share_task'),
    url(r'^completed_tasks/', tracker_app.views.completed_tasks, name='completed_tasks'),
]
