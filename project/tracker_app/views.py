from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import loader
from django.urls import reverse
from django.utils import timezone

import re
import datetime as dt

from tracker_app.forms import(
	ListForm,
	DivErrorList,
	UserForm,
	LoginForm,
	ShareForm,
	TaskForm,
)
from tracker_app.models import( 
	Task,
	User,
	TaskList,
	ArchiveTask,
)

def check_auth(request, module_name, user_data=True):
	"""Check user auth and load user data if 'user_data' flag is True"""
	context = {
		'current_user': None,
		'module_name': module_name,
		'time': timezone.now, 
	}
	if request.session.get('cuser_login', None) and request.session.get('cuser_password', None):
		try:
			ulogin = request.session['cuser_login']
			upassword = request.session['cuser_password'] 
			user = User.objects.get(login=ulogin)
			if user.password != upassword:
				raise ValueError('password')
		except:
			del request.session['cuser_login']
			del request.session['cuser_password']
		else: 
			context['current_user'] = user
			if user_data:
				context['tasks'] = user.tasks.all()
				context['tlists'] = user.tlists.all()
				context['sh_tasks'] = user.task_set.all()
				context['sh_tlists'] = user.tasklist_set.all()
				context['atasks'] = user.archive.all()
	if request.session.get('success_message', None):
		context['success_message'] = request.session['success_message']
		del request.session['success_message']
	return context

def home(request):
	"""Rendering home page"""
	context = check_auth(request, 'home')
	return render(request, 'home.html', context)

def registration(request):
	"""Registation proccessing and rendering page"""
	context = check_auth(request, 'registration', user_data=False)
	form = UserForm(request.POST or None, error_class=DivErrorList, label_suffix='')
	if request.POST and form.is_valid():
		user = form.save()
		request.session['cuser_login'] = user.login
		request.session['cuser_password'] = user.password
		return HttpResponseRedirect(reverse('home'))
	context['form'] = form 
	return render(request, 'registration.html', context)

def login(request):
	"""Log In proccessing and rendering page"""
	context = check_auth(request, 'log in')
	form = LoginForm(request.POST or None, error_class=DivErrorList, label_suffix='')
	if request.POST and form.is_valid():
		if request.POST.get('login', None):
			user = User.objects.get(pk=request.POST['login'])
			if user.password == request.POST['password']:
				request.session['cuser_login'] = user.login
				request.session['cuser_password'] = user.password
				return HttpResponseRedirect(reverse('home'))
		context['error_message'] = 'Incorrect password!'
	context['form'] = form
	return render(request, 'login.html', context)

def logout(request):
	"""Log Out proccessing"""
	if request.session.get('cuser_login', None) and request.session.get('cuser_password', None):
		del request.session['cuser_login']
		del request.session['cuser_password']
	return HttpResponseRedirect(reverse('home'))

def task_action(request, task_id):
	"""Proccessing post query and do task actions"""
	task = get_object_or_404(Task, pk=task_id)
	if request.POST.get('edit', None):
		return HttpResponseRedirect(reverse('edit_task', args=(task.id,)))
	elif request.POST.get('complete', None):
		return complete(request, task_id)
	elif request.POST.get('share', None):
		return HttpResponseRedirect(reverse('share_task', args=(task.id,)))
	elif request.POST.get('add_subtask', None):
		return HttpResponseRedirect(reverse('subtask', args=(task.id,)))
	elif request.POST.get('detail', None):
		return HttpResponseRedirect(reverse('detail', args=(task.id,)))
	elif request.POST.get('remove', None):
		return remove_task(request, task_id)
	else:
		return HttpResponseRedirect(reverse('home'))

def share_task(request, task_id):
	"""Share task with other users"""
	context = check_auth(request, 'share task', user_data=False)
	if context['current_user'] is None:
		return HttpResponseRedirect(reverse('home'))
	cuser = context['current_user']
	task = Task.objects.get(pk=task_id)
	form = ShareForm(request.POST or None, error_class=DivErrorList, label_suffix='')
	if request.POST and form.is_valid():
		user = User.objects.get(pk=request.POST['login'])
		if cuser.login != user.login:
			user.task_set.add(task)
			user.save()
			return HttpResponseRedirect(reverse('home'))
		context['error_message'] = 'Can\'t share with self!'  
	context['form'] = form
	context['task'] = task
	return render(request, 'share_task.html', context)

def complete(request, task_id):
	"""Complete task"""
	task = get_object_or_404(Task, pk=task_id)
	atask = ArchiveTask.objects.create(
		name=task.name,
		user_login=request.session['cuser_login'],
		complete_date=dt.datetime.now()
	)
	for user in task.get_all_users():
		user.archive.add(atask)
		user.save()
	if task.has_period():
		wd = task.get_boolwd()
		wday = dt.datetime.weekday(task.deadline)
		wd_id = None
		next_week = False
		for weekday in range(wday+1,7):
			if wd[weekday]: wd_id = weekday
		if wd_id is None:
			for weekday in range(0, wday+1):
				if wd[weekday]:
					wd_id = weekday
					next_week = True
		spend_days = wd_id
		if next_week:
			spend_days = spend_days + (6 - wday)
		delta = dt.timedelta(days=spend_days)
		task.deadline = task.deadline + delta
		task.deadline = dt.datetime(
			task.deadline.year,
			task.deadline.month,
			task.deadline.day,
			task.period_time.hour,
			task.period_time.minute,
		)
		task.save()
	else:
		task.delete()
	return HttpResponseRedirect(reverse('home'))

def edit_task(request, task_id=None, stask_id=None, tlist_id=None):
	"""Add task, add subtask, add task in task list proccessing, rendering"""
	context = check_auth(request, 'add task', user_data=False)
	stask = None
	if stask_id:
		stask = get_object_or_404(Task, pk=stask_id)
		context['stask_id'] = stask.id
		context['module_name'] = 'add subtask'
	tlist = None
	if tlist_id:
		tlist = get_object_or_404(TaskList, pk=tlist_id)
		context['tlist_id'] = tlist.id
	if task_id:
		task = get_object_or_404(Task, pk=task_id)
		context['module_name'] = 'edit task'
		context['task_id'] = task.id
		context['module_name'] = 'edit'
		if context['current_user'] is None or context['current_user'].login != task.owner().login:
			if not context['current_user'] in task.share_users.all(): 
				return HttpResponseRedirect(reverse('home'))
	else:
		task = Task(tlist=tlist, supertask=stask)
	form = TaskForm(request.POST or None, error_class=DivErrorList, label_suffix='', instance=task)
	if request.POST and form.is_valid():
		wd = request.POST.getlist('weekdays')
		ptime = request.POST['period_time']
		deadline = request.POST['deadline']
		if (len(wd) == 0 and ptime == '') or (len(wd) > 0 and ptime != '' and deadline != ''):
			task = form.save()
			if task.tlist is None and task.supertask is None:
				user = context['current_user']
				user.tasks.add(task)
				user.save()
			return HttpResponseRedirect(reverse('home'))
		context['error_message'] = 'Input period time and weekday!'
	context['form'] = form
	return render(request, 'add_task.html', context)

def remove_task(request, task_id):
	"""Remove task proccessing"""
	rtask = get_object_or_404(Task, pk=task_id)
	request.session['success_message'] = 'Task with name "{}" successfully removed'.format(rtask.name)  
	rtask.delete()
	return HttpResponseRedirect(reverse('home'))

def detail(request, task_id):
	"""Rendering detail task page"""
	task = get_object_or_404(Task, pk=task_id)
	context = check_auth(request, 'detail task', user_data=False)
	if context['current_user'] is None:
		return HttpResponseRedirect(reverse('home'))
	context['task'] = task
	return render(request, 'detail_task.html', context)

def completed_tasks(request):
	"""Rendering completed tasks page"""
	context = check_auth(request, 'share task list')
	return render(request, 'completed_tasks.html', context)

def tlist_action(request, tlist_id):
	"""Proccessing post query and do task list actions"""
	tlist = get_object_or_404(TaskList, pk=tlist_id)
	if request.POST.get('edit', None):
		return HttpResponseRedirect(reverse('edit_tlist', args=(tlist.id,)))
	elif request.POST.get('add_task', None):
		return HttpResponseRedirect(reverse('in_tlist', args=(tlist.id,)))
	elif request.POST.get('remove', None):
		return remove_tlist(request, tlist_id)
	elif request.POST.get('share', None):
		return HttpResponseRedirect(reverse('share_tlist', args=(tlist.id,)))
	else:
		return HttpResponseRedirect(reverse('home'))

def remove_tlist(request, tlist_id):
	"""Remove task list proccessing"""
	rtlist = get_object_or_404(TaskList, pk=tlist_id)
	request.session['success_message'] = 'Task List with name "{}" successfully removed.'.format(rtlist.name)
	rtlist.delete()	
	return HttpResponseRedirect(reverse('home'))

def share_tlist(request, tlist_id=None):
	"""Share task list proccessing and rendering page"""
	context = check_auth(request, 'share task list', user_data=False)
	if context['current_user'] is None:
		return HttpResponseRedirect(reverse('home'))
	cuser = context['current_user']
	tlist = TaskList.objects.get(pk=tlist_id)
	form = ShareForm(request.POST or None, error_class=DivErrorList, label_suffix='')
	if request.POST and form.is_valid():
		user = User.objects.get(pk=request.POST['login'])
		if cuser.login != user.login:
			user.tasklist_set.add(tlist)
			user.save()
			return HttpResponseRedirect(reverse('home'))
		context['error_message'] = 'Can\'t share with self!'  
	context['form'] = form
	context['tlist'] = tlist
	return render(request, 'share_tlist.html', context)

def edit_tlist(request, tlist_id=None):
	"""Add task list, edit task list proccessing and rendering"""
	context = check_auth(request, 'add task list', user_data=False)
	if context['current_user'] is None:
		return HttpResponseRedirect(reverse('home'))
	if tlist_id:
		tlist = get_object_or_404(TaskList, pk=tlist_id)
		context['module_name'] = 'edit task list'
		context['tlist_id'] = tlist.id
	else:
		tlist = TaskList()
	form = ListForm(request.POST or None, error_class=DivErrorList, instance=tlist)
	if request.POST and form.is_valid():
		user = User.objects.get(pk=request.session['cuser_login'])
		tlist = form.save()	
		user.tlists.add(tlist)
		user.save()
		return HttpResponseRedirect(reverse('home'))
	context['form'] = form
	return render(request, 'add_tlist.html', context)
