from django.db import models
from django.utils import timezone
from django import forms


class WeekDay(models.Model):
	"""Using for period task, contain all week days"""
	MO = 'MO'
	TU = 'TU'
	WE = 'WE'
	TH = 'TH'
	FR = 'FR'
	SA = 'SA'
	SU = 'SU'
	weekdays = (
		(MO, 'Monday'),
		(TU, 'Tuesday'),
		(WE, 'Wednesday'),
		(TH, 'Thursday'),
		(FR, 'Friday'),
		(SA, 'Saturday'),
		(SU, 'Sunday'),
	) 
	name = models.CharField(primary_key=True, choices=weekdays, max_length=9)

	def __str__(self):
		return self.get_name_display()


class Task(models.Model):
	"""Describes the task model"""
	name = models.CharField(max_length=255)
	creation_date = models.DateTimeField(auto_now_add=True)
	last_update = models.DateTimeField(auto_now=True)
	deadline = models.DateTimeField(blank=True, null=True, help_text='datetime in format: \'mm/dd/yyyy HH:MM\'')
	period_time = models.TimeField(blank=True, null=True, help_text='time in format: \'HH:MM\'')
	description = models.TextField(default='')
	supertask = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
	tlist = models.ForeignKey('TaskList', on_delete=models.CASCADE, blank=True, null=True, verbose_name='Task List')
	share_users = models.ManyToManyField('User')
	weekdays = models.ManyToManyField(WeekDay, blank=True)

	def has_period(self):
		return self.weekdays.count() > 0

	def get_boolwd(self):
		check = [WeekDay.MO, WeekDay.TU, WeekDay.WE, WeekDay.TH, WeekDay.FR, WeekDay.SA, WeekDay.SU]
		wd = [day.name for day in self.weekdays.all()]
		res = [day in wd for day in check]
		return res

	def get_all_users(self):
		tusers = [self.owner()]
		t1 = [user for user in self.share_users.all()]
		if self.tlist:
			t2 = [user for user in self.tlist.share_users.all()]
		for user in t1:
			if user:
				tusers.append(user for user in sel)
		if self.tlist:
			for user in t2:
				if user:
					tusers.append(user)
		return tusers

	def owner(self):
		if self.tlist:
			return [user for user in self.tlist.user_set.all()][0]
		elif self.supertask:
			if self.supertask.user_set.count() > 0:
				return [user for user in self.supertask.user_set.all()][0]
			else:
				return self.supertask.owner()
		return [user for user in self.user_set.all()][0]

	def __str__(self):
		return self.name


class TaskList(models.Model):
	"""Describes the task list model"""
	name = models.CharField(max_length=255)
	share_users = models.ManyToManyField('User')

	def __str__(self):
		return self.name

class User(models.Model):
	"""Describes the user model"""
	login = models.SlugField(max_length=20, primary_key=True)
	password = models.CharField(max_length=20)	
	tasks = models.ManyToManyField('Task')
	tlists = models.ManyToManyField('TaskList')
	archive = models.ManyToManyField('ArchiveTask')

	def __str__(self):
		return self.login

class ArchiveTask(models.Model):
	"""Describes completed task"""
	name = models.CharField(max_length=255)
	user_login = models.CharField(max_length=255)
	complete_date = models.DateTimeField(blank=True, null=True)
