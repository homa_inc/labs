from django import forms
from django.forms.utils import ErrorList
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from tracker_app.models import Task, TaskList, User, WeekDay  

def validate_user_existence(value):
	"""Function for user login validating, checking user exist"""
	try:
		User.objects.get(pk=value)
	except:
		raise ValidationError(
			_('User with login "%(value)s" does not exist!'),
			params={'value': value},
		)

class DivErrorList(ErrorList):
	"""Configurate style of errors in forms"""
	def __str__(self):
		return self.as_div()

	def as_div(self):
		if not self:
			return ''
		return '<small class="text-danger"><ul>%s</ul></small>' % ''.join(
			['<li>%s</li>' % e for e in self]
		)


class ListForm(forms.ModelForm):
	"""ModelForm of Task List"""
	class Meta:
		model = TaskList
		fields = ['name']
		widgets = {
			'name': forms.TextInput(attrs={'class': 'form-control'}),
		}


class UserForm(forms.ModelForm):
	"""ModelForm of User"""
	class Meta:
		model = User
		fields = ['login', 'password']
		widgets = {
			'login': forms.TextInput(attrs={'class': 'form-control', 'minlength': 5}),
			'password': forms.PasswordInput(attrs={'class': 'form-control', 'minlength': 6}),
		}


class LoginForm(forms.Form):
	"""Form for Registration and Log In"""
	login = forms.SlugField(
		max_length=20,
		widget=forms.TextInput(attrs={'class': 'form-control', 'minlength': 5}),
		validators=[validate_user_existence],
	)
	password = forms.CharField(max_length=20, widget=forms.PasswordInput(attrs={'class': 'form-control', 'minlength': 6}))


class ShareForm(forms.Form):
	"""Form for sharing Task or Task List"""
	login = forms.SlugField(
		max_length=20,
		widget=forms.TextInput(attrs={'class': 'form-control', 'minlength': 5}),
		validators=[validate_user_existence],
	)


class TaskForm(forms.ModelForm):
	"""ModelForm of Task"""
	class Meta:
		model = Task
		fields = ['name', 'tlist', 'deadline', 'period_time', 'weekdays', 'description']
		widgets = {
			'name': forms.TextInput(attrs={'class': 'form-control'}),
			'tlist': forms.Select(choices=TaskList.objects.all(), attrs={'class': 'form-control'}),
			'deadline': forms.DateTimeInput(format='%m/%d/%Y %H:%M', attrs={'class': 'form-control'}),
			'period_time': forms.TimeInput(format='%H:%M', attrs={'class': 'form-control'}),
			'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 3}),
			'weekdays': forms.SelectMultiple(choices=WeekDay.objects.all(), attrs={'class': 'form-control'}),
		}
